#
#    Copyright (C) 2016 mtsio <mtsio@cryptolab.net>
#    Copyright (C) 2016 Majin_Buu <seimirchaplin@openmailbox.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

from distlib import locators
from bisect import bisect_left

def check_package(package):
    """Checks if a package from PyPI is free and GPL-compatible.
    Returns -1 if the package isn't free, 0 if it's free but GPL-incompatible and 1 if it's free and GPL-compatible"""
    print package
    is_free = -1
    gpl_compatible = ('Apache Software License', 'Artistic License',
                     'BSD License', 'CC0 1.0 Universal (CC0 1.0) Public Domain Dedication',
                     'CEA CNRS Inria Logiciel Libre License, version 2.1'
                     '(CeCILL-2.1)', 'Eiffel Forum License',
                     'GNU Affero General Public License v3',
                     'GNU Affero General Public License v3 or later (AGPLv3+)',
                     'GNU Free Documentation License (FDL)',
                     'GNU General Public License (GPL)',
                     'GNU General Public License v2 (GPLv2)',
                     'GNU General Public License v2 or later (GPLv2+)',
                     'GNU General Public License v3 (GPLv3)',
                     'GNU General Public License v3 or later (GPLv3+)',
                     'GNU Lesser General Public License v2 (LGPLv2)',
                     'GNU Lesser General Public License v2 or later (LGPLv2+)',
                     'GNU Lesser General Public License v3 (LGPLv3)',
                     'GNU Lesser General Public License v3 or later (LGPLv3+)',
                     'GNU Library or Lesser General Public License (LGPL)',
                     'ISC License (ISCL)', 'MIT License',
                     'Mozilla Public License 2.0 (MPL 2.0)',
                     'Python License (CNRI Python License)',
                     'Python Software Foundation License', 'Sleepycat License',
                     'University of Illinois/NCSA Open Source License',
                     'W3C License', 'Zope Public License',
                     'zlib/libpng License')
    gpl_incompatible = ('Academic Free License (AFL)', 'Apple Public Source License',
                        'Common Public License', 'European Union Public Licence 1.0 (EUPL 1.0)',
                        'European Union Public Licence 1.1 (EUPL 1.1)',
                        'IBM Public License', 'Mozilla Public License 1.0 (MPL)',
                        'Mozilla Public License 1.1 (MPL 1.1)', 'Sun Public License')
    locator = locators.PyPIJSONLocator('https://pypi.python.org/pypi/')
    info = locator._get_project(package)
    #print info
    if not info:
        #print "not info"
        return -1

    version = info.keys()[0]
    license = info[version].metadata.license
    print license
    classifiers = info[version].metadata.classifiers
    #print classifiers
    # Check categories for a free license
    osi_approved, finish = 'License :: OSI Approved :: ', False
    for cl in classifiers:
        if cl[0:len(osi_approved)] == osi_approved:
            finish = True
            license_type = cl[27:]
            print license_type
            license_index = bisect_left(gpl_compatible, license_type)
            if license_index != len(gpl_compatible) and gpl_compatible[license_index] == license_type:
                new = check_license(license, license_type, True)
                if new == 1: return 1
                if new > is_free: is_free = new
            license_index = bisect_left(gpl_incompatible, license_type)
            if license_index != len(gpl_incompatible) and gpl_incompatible[license_index] == license_type:
                new = check_license(license, license_type, False)
                if new == 0: return 0
        else:
            if finish == True: break
    #If there's no license information in the classifiers, we need to search in the "License" field
    gpl_compatible = ('BSD', 'CC0', 'CeCILL', 'GNU', 'GPL', 'ISC', 'MPL', 'W3C')
    for cl in gpl_compatible:
        if cl in license: return 1
    gpl_incompatible = ('AFL', 'EUPL', 'IBM')
    for cl in gpl_incompatible:
        if cl in license: return 0
    return is_free

def check_license(license, license_type, gpl):
    if license_type in ('Artistic License', 'Eiffel Forum License'):
        if check_version(license, '2'): return 1
        return -1
    if license_type in ('Python License (CNRI Python License)', 'Python Software Foundation License'):
        if check_version(license, '1.6b1', '2.0', '2.1'): return 0
        return 1
    if license_type == 'Apple Public Source License':
        if check_version(license, '2'): return 0
        return -1
    
    if gpl == True: return 1
    return 0

def check_version(license, *versions):
    for v in versions:
        if v in license:
            return True
    
    return False